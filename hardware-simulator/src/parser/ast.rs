use crate::chip;
use nom;
use std::collections::HashMap;

#[derive(Debug, PartialEq)]
pub struct Chip<'a> {
    pub name: &'a str,
    pub inputs: Vec<&'a str>,
    pub outputs: Vec<&'a str>,
    pub parts: Vec<Part<'a>>,
}

#[derive(Debug, PartialEq)]
pub struct Part<'a> {
    pub name: &'a str,
    pub connections: HashMap<&'a str, &'a str>,
}

// TODO merge this with the builder?
impl<'a> Chip<'a> {
    /// Builds the AST Chip into a useable Chip.
    pub fn build(&self) -> Result<(&'a str, chip::Chip<'a>), nom::ErrorKind> {
        let mut c = chip::Chip::new();
        for input in &self.inputs {
            c = c.add_component(chip::Component::Input(input));
        }
        for output in &self.outputs {
            c = c.add_component(chip::Component::Output(output));
        }

        // TODO add context to these errors
        // TODO assert the map contains no other connections than those required
        for part in &self.parts {
            c = c.add_component(match part.name {
                "And" => chip::Component::And {
                    input_1: part.connections.get("a").ok_or(nom::ErrorKind::Custom(2))?,
                    input_2: part.connections.get("b").ok_or(nom::ErrorKind::Custom(2))?,
                    output: part.connections
                        .get("out")
                        .ok_or(nom::ErrorKind::Custom(2))?,
                },
                "Not" => chip::Component::Not {
                    input: part.connections.get("in").ok_or(nom::ErrorKind::Custom(2))?,
                    output: part.connections
                        .get("out")
                        .ok_or(nom::ErrorKind::Custom(2))?,
                },
                _ => unimplemented!(),
            })
        }

        // TODO don't lose this error
        Ok((self.name, c.build().map_err(|_| nom::ErrorKind::Custom(3))?))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_ast_build {
        ($name:ident, IN[$($inputs:expr),*], OUT[$($outputs:expr),*], PARTS[$(($($parts:tt)*)),* $(,)*]) => {
            #[test]
            fn $name() {
                let ast_chip = Chip {
                    name: stringify!($name),
                    inputs: vec![$($inputs),*],
                    outputs: vec![$($outputs),*],
                    parts: vec![$(test_ast_build!(@part $($parts)*)),*],
                };

                let (name, chip) = ast_chip.build().unwrap();
                let inputs: &[&str] = &[$($inputs),*];
                let outputs: &[&str] = &[$($outputs),*];

                assert_eq!(stringify!($name), name);
                assert_eq!(inputs, chip.input_labels());
                assert_eq!(outputs, chip.output_labels());
                //TODO check parts match
            }
        };
        (@part $name:expr, [$(($($component:tt)*)),*]) => {{
            let mut connections = HashMap::new();
            $(test_ast_build!(@component connections, $($component)*);)*
            Part { name: $name, connections }
        }};
        (@component $hashmap:expr, $label:expr) => {
            test_ast_build!(@component $hashmap, $label, $label)
        };
        (@component $hashmap:expr, $label:expr, $dest:expr) => {
            if $hashmap.insert($label, $dest).is_some() {
                panic!("duplicate label '{}' inserted", $label)
            }
        };
    }

    test_ast_build!(minimal, IN[], OUT[], PARTS[]);
    test_ast_build!(input1, IN["a"], OUT[], PARTS[]);
    test_ast_build!(input2, IN["a", "b"], OUT[], PARTS[]);
    test_ast_build!(output1, IN[], OUT["a"], PARTS[]);
    test_ast_build!(output2, IN[], OUT["a", "b"], PARTS[]);
    test_ast_build!(not, IN["in"], OUT["out"], PARTS[("Not", [("in"), ("out")])]);
}
