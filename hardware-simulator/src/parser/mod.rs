use std::collections::HashMap;

mod ast;

use nom::{self, multispace, not_line_ending, alphanumeric1, types::CompleteStr};

named!(pub parse<CompleteStr<'_>, ast::Chip<'_>>, do_parse!(
    token_separator        >>
    tag!("CHIP")           >> token_separator >>
    name: alphanumeric1    >> token_separator >>
    tag!("{")              >> token_separator >>
    inputs: opt!(inputs)   >> token_separator >>
    outputs: opt!(outputs) >> token_separator >>
    tag!("PARTS:")         >> token_separator >>
    parts: opt!(parts)     >> token_separator >>
    tag!("}")              >> token_separator >>
    (ast::Chip {
        name: name.0,
        inputs: inputs.unwrap_or_else(Vec::new),
        outputs: outputs.unwrap_or_else(Vec::new),
        parts: parts.unwrap_or_else(Vec::new),
    })
));

named!(parts<CompleteStr<'_>, Vec<ast::Part<'_>>>, many0!(delimited!(
    token_separator,
    part,
    token_separator
)));

named!(part<CompleteStr<'_>, ast::Part<'_>>, do_parse!(
    name: alphanumeric1      >> token_separator >>
    tag!("(")                >> token_separator >>
    connections: connections >> token_separator >>
    tag!(")")                >> token_separator >>
    tag!(";")                >> 
    (ast::Part {name: name.0, connections})
));

named!(
    ident<CompleteStr<'_>, &str>,
    map!(take_while!(|c| {nom::is_alphanumeric(c as u8) || c == '_'}), |r: CompleteStr<'_>| r.0)
);

named!(
    connection<CompleteStr<'_>, (&str, &str)>,
    do_parse!(
        label: ident >> token_separator >>
        tag!("=")   >> token_separator >>
        dest: ident   >>
        (label, dest)
    )
);

named!(
    connections<CompleteStr<'_>, HashMap<&str, &str>>, map_res!(
        separated_nonempty_list!(
            delimited!(token_separator, char!(','), token_separator),
            connection
        ),
        vec_to_hashmap
    )
);

fn vec_to_hashmap<'a, 'b>(
    list: Vec<(&'a str, &'b str)>,
) -> Result<HashMap<&'a str, &'b str>, nom::ErrorKind> {
    let mut hashmap = HashMap::new();

    for (label, dest) in list {
        if hashmap.insert(label, dest).is_some() {
            return Err(nom::ErrorKind::Custom(1));
        }
    }

    Ok(hashmap)
}

named!(inputs<CompleteStr<'_>, Vec<&str>>, do_parse!(
    tag!("IN") >> token_separator >>
    outputs: in_out_list >>
    (outputs)
));

named!(outputs<CompleteStr<'_>, Vec<&str>>, do_parse!(
    tag!("OUT")          >> token_separator >>
    outputs: in_out_list >>
    (outputs)
));

named!(in_out_list<CompleteStr<'_>, Vec<&str>>, do_parse!(
    items: opt!(separated_list!(
        delimited!(token_separator, tag!(","), token_separator),
        ident
    )) >> token_separator >>
    tag!(";")  >>
    (items.unwrap_or_else(|| vec![]))
));

/// Parses single line comments that begin with `//` and finish that the end of the line.
named!(comment<CompleteStr<'_>, CompleteStr<'_>>, preceded!(tag!("//"), not_line_ending));

/// Parses block comments between the tags `/*` and `*/`.
named!(
    comment_block<CompleteStr<'_>, CompleteStr<'_>>,
    delimited!(tag!("/*"), take_until!("*/"), tag!("*/"))
);

/// Space between tokens, can be whitespace, nothing or a comment
named!(
    token_separator<CompleteStr<'_>, ()>,
    do_parse!(
        many0!(alt!(comment | comment_block | multispace)) >> ()
    )
);

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_hdl_parser {
        ($name:ident, IN[$($inputs:expr),*], OUT[$($outputs:expr),*], PARTS[$(($($parts:tt)*)),* $(,)*]) => {
            #[test]
            fn $name() {
                let input = include_str!(concat!("test/", stringify!($name), ".hdl"));
                assert_eq!(
                    parse(CompleteStr(&input)),
                    Ok((CompleteStr(""), ast::Chip {
                        name: stringify!($name),
                        inputs: vec![$($inputs),*],
                        outputs: vec![$($outputs),*],
                        parts: vec![$(test_hdl_parser!(@part $($parts)*)),*],
                    }))
                );
            }
        };
        (@part $name:expr, [$(($($component:tt)*)),*]) => {{
            let mut connections = HashMap::new();
            $(test_hdl_parser!(@component connections, $($component)*);)*
            ast::Part { name: $name, connections }
        }};
        (@component $hashmap:expr, $label:expr) => {
            test_hdl_parser!(@component $hashmap, $label, $label)
        };
        (@component $hashmap:expr, $label:expr, $dest:expr) => {
            if $hashmap.insert($label, $dest).is_some() {
                panic!("duplicate label '{}' inserted", $label)
            }
        };
    }

    test_hdl_parser!(minimal,          IN[], OUT[], PARTS[]);
    test_hdl_parser!(minimalcomments1, IN[], OUT[], PARTS[]);
    test_hdl_parser!(minimalcomments2, IN[], OUT[], PARTS[]);
    test_hdl_parser!(minimalcomments3, IN[], OUT[], PARTS[]);

    test_hdl_parser!(inputs1, IN[], OUT[], PARTS[]);
    test_hdl_parser!(inputs2, IN["a"], OUT[], PARTS[]);
    test_hdl_parser!(inputs3, IN["a", "b"], OUT[], PARTS[]);
    test_hdl_parser!(inputs4, IN["a", "b"], OUT[], PARTS[]);

    test_hdl_parser!(outputs1, IN[], OUT[], PARTS[]);
    test_hdl_parser!(outputs2, IN[], OUT["out"], PARTS[]);
    test_hdl_parser!(outputs3, IN[], OUT["o1", "o2"], PARTS[]);
    test_hdl_parser!(outputs4, IN[], OUT["o1", "o2"], PARTS[]);

    test_hdl_parser!(not, IN["in"], OUT["out"], PARTS[
        ("Not", [("in"), ("out")]),
    ]);
    test_hdl_parser!(and, IN["a", "b"], OUT["out"], PARTS[
        ("And", [("a"), ("b"), ("out")]),
    ]);
    test_hdl_parser!(multipart1, IN["a", "b"], OUT["out"], PARTS[
        ("And", [("a"), ("b"), ("out", "o1")]),
        ("Not", [("in", "o1"), ("out")]),
    ]);
    test_hdl_parser!(multipart2, IN["one", "two", "three", "four"], OUT["final"], PARTS[
        ("And", [("a", "one"), ("b", "two"), ("out", "inner1")]),
        ("And", [("a", "three"), ("b", "four"), ("out", "inner2")]),
        ("And", [("a", "inner1"), ("b", "inner2"), ("out", "final")]),
    ]);
    test_hdl_parser!(multipart3, IN["a", "b"], OUT["out"], PARTS[
        ("And", [("a", "oa"), ("b", "ob"), ("out")]),
        ("Not", [("in", "a"), ("out", "oa")]),
        ("Not", [("in", "b"), ("out", "ob")]),
    ]);
    test_hdl_parser!(multipart4, IN["input"], OUT["output"], PARTS[
        ("And", [("a", "inner3"), ("b", "inner1"), ("out", "output")]),
        ("Not", [("in", "input"), ("out", "inner1")]),
        ("Not", [("in", "input"), ("out", "inner2")]),
        ("Not", [("in", "inner2"), ("out", "inner3")]),
    ]);
    test_hdl_parser!(multipartcomments, IN["input"], OUT["output"], PARTS[
        ("And", [("a", "inner3"), ("b", "inner1"), ("out", "output")]),
        ("Not", [("in", "input"), ("out", "inner1")]),
        ("Not", [("in", "input"), ("out", "inner2")]),
        ("Not", [("in", "inner2"), ("out", "inner3")]),
    ]);

    macro_rules! test_comment{
        ($name:ident, $body:expr, $tail:expr) => {
            #[test]
            fn $name() {
                assert_eq!(
                    comment(CompleteStr(concat!("//", $body, "\n", $tail))),
                    Ok((CompleteStr(concat!("\n", $tail)), CompleteStr($body)))
                );
                assert_eq!(
                    comment(CompleteStr(concat!("//", $body, "\r\n", $tail))),
                    Ok((CompleteStr(concat!("\r\n", $tail)), CompleteStr($body)))
                );
            }
        };
    }

    test_comment!(test_comment_empty, "", "");
    test_comment!(test_comment_simple, "foo", "");
    test_comment!(test_comment_trailing, "", "foo");
    test_comment!(test_comment_both, "foo", "bar");
    test_comment!(test_comment_extra_slash, "///foo", "bar");
    test_comment!(test_comment_following_comment, "bar", "    //foo");
    test_comment!(test_comment_whitespace, "  \t\t \t foo  \t ", "  \t  \t ");
    test_comment!(test_comment_multi_line, "bar", "\r\n\r\n\r\n");
    test_comment!(test_comment_semi_colon, "bar", ";");

    macro_rules! test_multi_comment{
        ($name:ident, $body:expr, $tail:expr) => {
            #[test]
            fn $name() {
                assert_eq!(
                    comment_block(CompleteStr(concat!("/*", $body, "*/", $tail))),
                    Ok((CompleteStr($tail), CompleteStr($body))
                ));
            }
        };
    }

    test_multi_comment!(test_multi_comment_empty, "", "");
    test_multi_comment!(test_multi_comment_whitespace_1, " ", " ");
    test_multi_comment!(
        test_multi_comment_whitespace_2,
        " \r\n\r\n  \t  \t\r\n ",
        "\r\n  \r\n "
    );
    test_multi_comment!(test_multi_comment_simple, "Some text", "Trailing");
    test_multi_comment!(test_multi_comment_multiline, "Some \r\n text", "Trailing");
    test_multi_comment!(test_multi_comment_star, "Some \r\n * text", "Trailing");
    test_multi_comment!(
        test_multi_comment_nested_opening,
        "Some \r\n /* text",
        "Trailing"
    );
    test_multi_comment!(
        test_multi_comment_nested_tailing_block_comment,
        "Some \r\n /* text",
        "/**/Trailing"
    );
    test_multi_comment!(
        test_multi_comment_nested_tailing_comment,
        "Some \r\n /* text",
        "//Trailing"
    );

    macro_rules! test_token_separator_inner{
        ($body:expr, $($token:expr),*) => {
            $(assert_eq!(
                token_separator(CompleteStr(concat!($body, $token))),
                Ok((CompleteStr($token), ()))
            );)*
        };
    }

    macro_rules! test_token_separator {
        ($name:ident, $body:expr) => {
            #[test]
            fn $name() {
                test_token_separator_inner!(
                    $body, ";", "a", "foo", "(", "9", ")", "@", "'", "\""
                );
            }
        };
    }

    test_token_separator!(test_token_separator_empty, "");
    test_token_separator!(test_token_separator_space, " ");
    test_token_separator!(test_token_separator_tab, "\t");
    test_token_separator!(test_token_separator_newline, "\n");
    test_token_separator!(test_token_separator_return, "\r");
    test_token_separator!(test_token_separator_whitespace, "  \t\t\r\n\t  \r\n");
    test_token_separator!(test_token_separator_comment, "//\r\n");
    test_token_separator!(test_token_separator_comment_body, "// foobar\r\n");
    test_token_separator!(
        test_token_separator_two_comments,
        "// foobar\r\n// barfoo\r\n"
    );
    test_token_separator!(
        test_token_separator_multi_comments_with_blanks,
        "// foobar\r\n// barfoo\r\n\r\n\r\n   // baz\r\n"
    );
    test_token_separator!(test_token_separator_block_comments_simple, "/**/");
    test_token_separator!(
        test_token_separator_block_comments_body,
        "/* with a body */"
    );
    test_token_separator!(
        test_token_separator_block_comments_body_with_whitespace,
        "   /* with a body */   "
    );
    test_token_separator!(
        test_token_separator_block_comments_body_with_inner_newlines,
        "   /* with \r\na\r\n body */   "
    );
    test_token_separator!(
        test_token_separator_block_comments_body_with_outer_newlines,
        "\r\n/* with a body */\r\n\r\n  \r\n"
    );
    test_token_separator!(
        test_token_separator_everything,
        "// some comment\r\n/* with a body */\r\n\r\n\r\n\r\n  // foobar\r\n\r\n\t\t/**/\t  /* /* */  // sdsa    \t\r\n"
    );
}
