use std::collections::HashMap;
use std::rc::Rc;

#[derive(Debug, Fail, PartialEq)]
pub enum Error {
    #[fail(display = "wire already exists with the label '{}'", _0)] WireExists(String),
}

#[derive(Debug, Fail, PartialEq)]
#[fail(display = "a wire with the label '{}' does not exist", wire_label)]
pub struct ErrorWireDoesNotExist {
    wire_label: String,
}

/// A single gate, or sub chip. This is a building block of the Chip, each component has a
/// number of inputs and outputs that can be connected to labeled wires.
#[derive(Debug, PartialEq)]
pub enum Component<'a> {
    /// Ensures the wire exists, but does not change its value when evaling. These act as input
    /// wires and their inital values are passed in to the eval function or assumed false.
    Input(&'a str),
    /// Ensures the wire exists and is used to mark wires that are considered and output.
    Output(&'a str),
    /// Inverts the input.
    Not { input: &'a str, output: &'a str },
    /// Similar to the And gate with a Not gate on the output. Return false only when both the
    /// inputs are true.
    Nand {
        input_1: &'a str,
        input_2: &'a str,
        output: &'a str,
    },
    /// Returns true only when both the inputs are true.
    And {
        input_1: &'a str,
        input_2: &'a str,
        output: &'a str,
    },
    /// A nested chip, this gets resolved like the outter chip does and allows nesting of different
    /// chips to form more complex chips.
    SubChip(&'a Chip<'a>),
}

/// A chip that takes some input and produces some output via its eval method based on the internal
/// linking of its compoents.
#[derive(Debug, PartialEq)]
pub struct Chip<'a> {
    wires: HashMap<&'a str, Rc<Component<'a>>>,
    outputs: Box<[&'a str]>,
    inputs: Box<[&'a str]>,
}

/// Used to build a Chip, once built chips are immutable.
pub struct Builder<'a> {
    components: Vec<Component<'a>>,
}

/// Sets up input pins and allows the chip to be evaluated.
pub struct Evaluator<'a> {
    inner: EvalResult<'a>,
}

/// Represents a valid set of inputs, the wires will match labeled wires in the chip.
struct ValidInputs<'a> {
    chip: &'a Chip<'a>,
    wires: HashMap<&'a str, bool>,
}

/// The result of constructing an chip evaluation. This allows chaining of the evaluators methods
/// and shortcuts them if an Invalid(_) value is ever detected.
enum EvalResult<'a> {
    Valid(ValidInputs<'a>),
    Invalid(ErrorWireDoesNotExist),
}

/// The result of an evaluation.
pub struct Evaluation<'a> {
    chip: &'a Chip<'a>,
    wires: HashMap<&'a str, bool>,
}

impl<'a> Builder<'a> {
    /// Adds a component to the chip. Any invalid links will be returned when you attempt to build
    /// the chip.
    pub fn add_component(mut self, component: Component<'a>) -> Builder<'a> {
        self.components.push(component);
        self
    }

    /// Builds the chip from the added components. It ensures that each output label is unique and
    /// returns an error if there are two compoents with the same output label.
    pub fn build(self) -> Result<Chip<'a>, Error> {
        macro_rules! add_wires {
            ($wires:expr, $component:expr, $($label:expr),*) => {
                $(
                    if $wires.insert($label, $component).is_some() {
                        return Err(Error::WireExists($label.into()));
                    }
                )*
            }
        }
        use self::Component::*;

        let mut wires = HashMap::new();
        let mut outputs = Vec::new();
        let mut inputs = Vec::new();
        for component in self.components {
            match component {
                Input(label) => {
                    add_wires!(wires, Rc::new(component), label);
                    inputs.push(label);
                }
                Output(label) => {
                    outputs.push(label);
                }
                Not { output, .. } | Nand { output, .. } | And { output, .. } => {
                    add_wires!(wires, Rc::new(component), output);
                }
                SubChip(chip) => {
                    let component = Rc::new(component);
                    for label in chip.output_labels().iter() {
                        add_wires!(wires, component.clone(), *label);
                    }
                    inputs.extend_from_slice(chip.input_labels());
                    outputs.extend_from_slice(chip.output_labels());
                }
            }
        }
        Ok(Chip {
            wires,
            outputs: outputs.into_boxed_slice(),
            inputs: inputs.into_boxed_slice(),
        })
    }
}

impl<'a> Chip<'a> {
    pub fn new() -> Builder<'a> {
        Builder {
            components: Vec::new(),
        }
    }

    pub fn input_labels(&self) -> &[&'a str] {
        &self.inputs
    }

    pub fn output_labels(&self) -> &[&'a str] {
        &self.outputs
    }

    pub fn start_evaluation(&self) -> Evaluator<'_> {
        Evaluator {
            inner: EvalResult::Valid(ValidInputs {
                chip: &self,
                wires: self.inputs
                    .iter()
                    .map(|label| (*label, false))
                    .collect::<HashMap<_, _>>(),
            }),
        }
    }
}

impl<'a> ValidInputs<'a> {
    fn eval(&mut self, outputs: &[&'a str]) -> Result<(), ErrorWireDoesNotExist> {
        use self::Component::*;
        use std::borrow::Borrow;

        // Evaluate the components
        for output in outputs.iter() {
            let component = self.chip
                .wires
                .get(output)
                .cloned()
                // Outputs are not placed in the wire array as they don't own the wire, instead
                // other components do. But it is valid for a wire to be unconnected so here we use
                // Output as a placeholder for this. It will always resolve to false.
                // TODO make this more intuative
                // What we likely want to do is add a Flase component above and have that as the
                // only replacable component.
                .unwrap_or_else(|| Rc::new(Output(output)));
            match component.borrow() {
                Input(_) | Output(_) => {
                    self.wires.entry(output).or_insert(false);
                }
                Not { input, output } => {
                    self.eval(&[input])?;
                    let input = *self.wires.get(input).unwrap_or(&false);
                    *self.wires.entry(output).or_insert(false) = !input;
                }
                Nand {
                    input_1,
                    input_2,
                    output,
                } => {
                    self.eval(&[input_1, input_2])?;
                    let input_1 = *self.wires.get(input_1).unwrap_or(&false);
                    let input_2 = *self.wires.get(input_2).unwrap_or(&false);
                    *self.wires.entry(output).or_insert(false) = !(input_1 & input_2);
                }
                And {
                    input_1,
                    input_2,
                    output,
                } => {
                    self.eval(&[input_1, input_2])?;
                    let input_1 = *self.wires.get(input_1).unwrap_or(&false);
                    let input_2 = *self.wires.get(input_2).unwrap_or(&false);
                    *self.wires.entry(output).or_insert(false) = input_1 & input_2;
                }
                SubChip(chip) => {
                    let mut inputs = chip.start_evaluation();
                    self.eval(
                        match &inputs.inner {
                            EvalResult::Invalid(e) => panic!("{}", e),
                            EvalResult::Valid(i) => i.wires.keys().map(|v| *v).collect::<Vec<_>>(),
                        }.as_slice(),
                    )?;
                    for input_label in chip.input_labels() {
                        inputs = inputs.set(
                            input_label,
                            self.wires.get(input_label).cloned().unwrap_or(false),
                        );
                    }

                    let outputs = inputs.eval()?;
                    println!("SubChip wires after: {:?}", outputs.wires);

                    for output_label in chip.output_labels() {
                        *self.wires.entry(output_label).or_insert(false) =
                            outputs.get(output_label).unwrap();
                    }
                }
            }
        }
        Ok(())
    }
}

impl<'a> Evaluator<'a> {
    pub fn set(self, label: &'a str, value: bool) -> Evaluator<'a> {
        match self.inner {
            EvalResult::Valid(mut inputs) => {
                if let Some(wire) = inputs.wires.get_mut(label) {
                    *wire = value;
                } else {
                    return Evaluator {
                        inner: EvalResult::Invalid(ErrorWireDoesNotExist::new(label.into())),
                    };
                }
                Evaluator {
                    inner: EvalResult::Valid(inputs),
                }
            }
            EvalResult::Invalid(_) => self,
        }
    }

    pub fn input_labels(&self) -> Option<&[&'a str]> {
        match &self.inner {
            EvalResult::Valid(inputs) => Some(inputs.chip.input_labels()),
            EvalResult::Invalid(_) => None,
        }
    }

    pub fn eval(self) -> Result<Evaluation<'a>, ErrorWireDoesNotExist> {
        match self.inner {
            EvalResult::Valid(mut inputs) => {
                inputs.eval(&inputs.chip.outputs)?;
                Ok(Evaluation {
                    chip: inputs.chip,
                    wires: inputs.wires,
                })
            }
            EvalResult::Invalid(e) => Err(e),
        }
    }
}

impl<'a> Evaluation<'a> {
    pub fn get(&self, label: &str) -> Result<bool, ErrorWireDoesNotExist> {
        match self.wires.get(label) {
            Some(v) => Ok(*v),
            None => Err(ErrorWireDoesNotExist::new(label.into())),
        }
    }

    pub fn input_labels(&self) -> &[&'a str] {
        &self.chip.input_labels()
    }

    pub fn output_labels(&self) -> &[&'a str] {
        &self.chip.output_labels()
    }
}

impl ErrorWireDoesNotExist {
    fn new(wire_label: String) -> Self {
        Self { wire_label }
    }
}

#[cfg(test)]
mod tests {
    use super::{Chip, Component, ErrorWireDoesNotExist};
    use self::Component::*;

    macro_rules! component_test {
        (
            $name:ident;
            $( let $subchip_ident:ident = $subchip:expr; )*
            $chip:expr;
            $( $in_label:expr ),* => $( $out_label:expr ),*;
            $( $( $in_value:expr ),* => $( $out_value:expr ),* ;)*
        ) => {
            #[test]
            fn $name() {
                $(
                    let $subchip_ident = $subchip.build().expect("chip should be buildable");
                )*
                let chip = $chip.build().expect("chip should be buildable");
                let input_labels = vec![$($in_label),*];
                let output_labels = vec![$($out_label),*];
                $({
                    let inputs = input_labels.iter().zip(vec![$($in_value),*]);
                    let outputs = output_labels.iter().zip(vec![$($out_value),*]);
                    let mut eval = chip.start_evaluation();
                    for (label, value) in inputs {
                        println!("{} => {}", label, value);
                        eval = eval.set(label, value);
                    }
                    let actual_outputs = eval.eval().expect("eval should not error");
                    for (label, value) in outputs {
                        assert_eq!(value, actual_outputs.get(label).unwrap());
                    }
                })*
            }
        };
    }

    component_test!(
        component_no_components;
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out1"))
            .add_component(Output("out2"));
        "in1", "in2" => "out1", "out2";
        false, false => false, false;
        false, true  => false, false;
        true,  false => false, false;
        true,  true  => false, false;
    );

    component_test!(
        component_not;
        Chip::new()
            .add_component(Input("in"))
            .add_component(Output("out"))
            .add_component(Not{ input: "in", output: "out"});
        "in" => "out";
        false => true;
        true  => false;
    );

    component_test!(
        component_not_chained;
        Chip::new()
            .add_component(Input("in"))
            .add_component(Output("out"))
            .add_component(Not{ input: "in", output: "inner"})
            .add_component(Not{ input: "inner", output: "out"});
        "in" => "out";
        false => false;
        true  => true;
    );

    component_test!(
        component_not_fan_out;
        Chip::new()
            .add_component(Input("in"))
            .add_component(Output("out1"))
            .add_component(Output("out2"))
            .add_component(Not{ input: "in", output: "out1"})
            .add_component(Not{ input: "in", output: "out2"});
        "in" => "out1", "out2";
        false => true,  true;
        true  => false, false;
    );

    component_test!(
        component_not_parrallel;
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out1"))
            .add_component(Output("out2"))
            .add_component(Not{ input: "in1", output: "out1"})
            .add_component(Not{ input: "in2", output: "out2"});
        "in1", "in2" => "out1", "out2";
        false, false => true,   true;
        false, true  => true,   false;
        true,  false => false,  true;
        true,  true  => false,  false;
    );

    component_test!(
        component_nand;
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out"))
            .add_component(Nand{ input_1: "in1", input_2: "in2", output: "out"});
        "in1", "in2" => "out";
        false, false => true;
        false, true  => true;
        true,  false => true;
        true,  true  => false;
    );

    component_test!(
        component_and;
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out"))
            .add_component(And{ input_1: "in1", input_2: "in2", output: "out"});
        "in1", "in2" => "out";
        false, false => false;
        false, true  => false;
        true,  false => false;
        true,  true  => true;
    );

    component_test!(
        component_subchip_basic;
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out"))
            .add_component(And{ input_1: "in1", input_2: "in2", output: "out"});
        "in1", "in2" => "out";
        false, false => false;
        false, true  => false;
        true,  false => false;
        true,  true  => true;
    );

    component_test!(
        component_subchip_multiple_inner_components;
        let subchip = Chip::new()
            .add_component(And{ input_1: "in1", input_2: "in2", output: "o1"})
            .add_component(Not{ input: "in3", output: "o2"})
            .add_component(Nand{ input_1: "o1", input_2: "o2", output: "out"})
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Input("in3"))
            .add_component(Output("out"));
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Input("in3"))
            .add_component(Output("out"))
            .add_component(SubChip(&subchip));
        "in1", "in2", "in3" => "out";
        false, false, false => true;
        false, false, true  => true;
        false, true,  false => true;
        false, true,  true  => true;
        true,  false, false => true;
        true,  false, true  => true;
        true,  true,  false => false;
        true,  true,  true  => true;
    );

    component_test!(
        component_subchip_multiple_components;
        let subchip = Chip::new()
            .add_component(Not{ input: "in1", output: "inner1"})
            .add_component(And{ input_1: "inner1", input_2: "in2", output: "out2"})
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Output("out2"));
        Chip::new()
            .add_component(Input("in1"))
            .add_component(Input("in2"))
            .add_component(Input("in3"))
            .add_component(Output("out1"))
            .add_component(Output("out2"))
            .add_component(Output("out3"))
            .add_component(Not { input: "out2", output: "out1"})
            .add_component(And { input_1: "in1", input_2: "in3", output: "out3"})
            .add_component(SubChip(&subchip));
        "in1", "in2", "in3" => "out1", "out2", "out3";
        false, false, false => true,  false, false;
        false, false, true  => true,  false, false;
        false, true,  false => false, true,  false;
        false, true,  true  => false, true,  false;
        true,  false, false => true,  false, false;
        true,  false, true  => true,  false, true;
        true,  true,  false => true,  false, false;
        true,  true,  true  => true,  false, true;
    );

    #[test]
    fn missing_input_1() {
        let chip = Chip::new()
            .add_component(Input("in1"))
            .add_component(Not {
                input: "in1",
                output: "out",
            })
            .add_component(Output("out"))
            .build()
            .unwrap();

        if let Err(e) = chip.start_evaluation().set("in2", true).eval() {
            assert_eq!(e, ErrorWireDoesNotExist::new("in2".into()));
        } else {
            panic!("input should not exist");
        }
    }

    #[test]
    fn missing_input_2() {
        let chip = Chip::new()
            .add_component(Input("in1"))
            .add_component(Not {
                input: "in1",
                output: "out",
            })
            .add_component(Output("out"))
            .build()
            .unwrap();

        let outputs = chip.start_evaluation().set("in1", true).eval().unwrap();
        assert_eq!(
            outputs.get("aaa"),
            Err(ErrorWireDoesNotExist::new("aaa".into()))
        );
    }
}
