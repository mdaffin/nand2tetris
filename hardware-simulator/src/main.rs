
#[macro_use]
extern crate structopt;
use hardware_simulator::{parser, CompleteStr};

use structopt::StructOpt;
use std::path::PathBuf;
use std::fs::read_to_string;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opt {
    /// HDL file of the chip
    #[structopt(name = "HDL_FILE", parse(from_os_str))]
    hdl_file: PathBuf,
}

fn main() {
    let opt = Opt::from_args();

    let input = read_to_string(&opt.hdl_file).unwrap();
    let (_, chip) = parser::parse(CompleteStr(&input)).unwrap();
    let (name, chip) = chip.build().unwrap();

    println!("loaded {} from {}", name, opt.hdl_file.display());

    let outputs = chip.start_evaluation().set("in", true).eval().unwrap();
    println!("in: {:?}", &outputs.get("in").unwrap());
    println!("out: {:?}", &outputs.get("out").unwrap());
    assert_eq!(outputs.get("in"), Ok(true), "failed for wire 'in'");
    assert_eq!(outputs.get("out"), Ok(false), "failed for wire 'out'");

    let outputs = chip.start_evaluation().set("in", false).eval().unwrap();
    println!("in: {:?}", &outputs.get("in").unwrap());
    println!("out: {:?}", &outputs.get("out").unwrap());
    assert_eq!(outputs.get("in"), Ok(false), "failed for wire 'in'");
    assert_eq!(outputs.get("out"), Ok(true), "failed for wire 'out'");
}
